// Import the functions you need from the SDKs you need
import { initializeApp } from "firebase/app";
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

import { getAuth } from "firebase/auth";

// Your web app's Firebase configuration
const firebaseConfig = {
  apiKey: "AIzaSyBxPj7sOXjAv000wweb3eJK3F5-AtG8F_A",
  authDomain: "devcamp-r27-t.firebaseapp.com",
  projectId: "devcamp-r27-t",
  storageBucket: "devcamp-r27-t.appspot.com",
  messagingSenderId: "618040131052",
  appId: "1:618040131052:web:adabab7741c0afdfc814c9"
};

// Initialize Firebase
const app = initializeApp(firebaseConfig);

const auth = getAuth(app);

export default auth;